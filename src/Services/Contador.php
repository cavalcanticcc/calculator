<?php

namespace Atd\Calculator\Services;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Acao;
use App\Processo;
use App\Outorga;
use App\Preanalise;
use App\ProcessosCaixa;
use App\Pedidoedicaoprocesso;
use App\Aerodromo;
use App\Subcaixa;
use App\Fiadpista;
use Carbon\Carbon;
use App\PortalZpa;
use App\PortalTacAero;
use App\PortalTac;

class Contador
{

    static public function teste()
    {
        return 'ois';
    }

    public static function preanalisesArray()
    {
        $consulta = [];

        $consulta[0] = Preanalise::
        when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
            return
                $query->where('regional', auth()->user()->regional());
        })
            ->whereNull('deleted_at')
            ->whereNull('abrirProcesso')
            ->count();

        $consulta[1] = Preanalise::
        when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
            return
                $query->where('regional', auth()->user()->regional());
        })
            ->whereNull('deleted_at')
            ->where('abrirProcesso', 'nao')
            ->count();

        $consulta[2] = Preanalise::
        when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
            return
                $query->where('regional', auth()->user()->regional());
        })
            ->whereNull('deleted_at')
            ->where('abrirProcesso', 'sim')
            ->count();

        $consulta[3] = Preanalise::
        when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
            return
                $query->where('regional', auth()->user()->regional());
        })
            ->whereNull('deleted_at')
            ->where('abrirProcesso', 'corrigido')
            ->count();

        $consulta[4] = Preanalise::
        when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
            return
                $query->where('regional', auth()->user()->regional());
        })
            ->whereNull('deleted_at')
            ->where('abrirProcesso', 'processoAberto')
            ->count();
        return $consulta;
    }

    public static function Ad()
    {
        $hoje = Carbon::today();
        $consulta = Aerodromo::when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
            return
                $query->where('regional', auth()->user()->regional());
        })
            ->whereNotNull('dataLimite')
            ->whereDate('dataLimite', '<', $hoje->format('Y-m-d'))
            ->count();
        return $consulta;
    }

    public static function OutorgasPendentes()
    {
        $consulta = Outorga::when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
            return
                $query->WhereHas('aerodromo', function ($query3) {
                    $query3->where('regional', auth()->user()->regional());
                });
        })
            ->where('verificado', 'Pendente')
            ->count();
        return $consulta;
    }

    public static function AdTac()
    {
        $hoje = Carbon::today();

        try {
            $consulta = PortalTacAero::with('tac')
                ->WhereHas('tac', function ($query3) use ($hoje) {
                    $query3->WhereNull('deletedat')
                        ->whereDate('dt_validade', '>=', $hoje->format('Y-m-d'));
                })
                ->count();
        } catch (\Exception $e) {
            $consulta = 0;
        }

        return $consulta;
    }

    public static function AdZPAAD()
    {
        $hoje = Carbon::today();

        $consulta = [];

        try {
            $consultaTotal = PortalZpa::whereNull('deletedat')->get();

//        $consultaTotal = PortalZpa::whereNotNull('ciad')->where('ciad', '!=', '')->get();

            $consulta[0] = $consultaTotal->filter(function ($value, $key) {
                return $value->ciad != null;
            })->count();
            $consulta[1] = $consultaTotal->filter(function ($value, $key) {
                return $value->ciad == null;
            })->count();
        } catch (\Exception $e) {
            $consulta = [0, 0];
        }


        return $consulta;
    }

    public static function AdTacVencido()
    {

        $hoje = Carbon::today();

        try {
            $consulta = PortalTacAero::with('tac')
                ->WhereHas('tac', function ($query3) use ($hoje) {
                    $query3->WhereNull('deletedat')
                        ->whereDate('dt_validade', '<', $hoje->format('Y-m-d'));;
                })
                ->count();
        } catch (\Exception $e) {
            $consulta = 0;
        }


        return $consulta;
    }

    public static function PedidosEncerrar()
    {

        $consulta = ProcessosCaixa::with('processo')
            ->when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
                return
                    $query->WhereHas('processo', function ($query3) {
                        $query3->where('regional', auth()->user()->regional());
                    });
            })
            ->where('idCaixa', 1)
            ->where('explicacaoEntrada', 'Pedida de encerramento de processo.')
            ->whereNull('data_saida')
            ->count();
        return $consulta;
    }

    public static function TerminoObra()
    {

        $consulta = ProcessosCaixa::with('processo')
            ->when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
                return
                    $query->WhereHas('processo', function ($query3) {
                        $query3->where('regional', auth()->user()->regional());
                    });
            })
            ->where('idCaixa', 1)
            ->where('explicacaoEntrada', 'Apresentadado término de obra.')
            ->whereNull('data_saida')
            ->count();
        return $consulta;
    }

    public static function PedidosEditar()
    {

        $consulta = ProcessosCaixa::with('processo')
            ->when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
                return
                    $query->WhereHas('processo', function ($query3) {
                        $query3->where('regional', auth()->user()->regional());
                    });
            })
            ->where('idCaixa', 1)
            ->where('explicacaoEntrada', 'Pedida de edição de processo.')
            ->whereNull('data_saida')
            ->count();
        return $consulta;
    }

    public static function NotificacoesVistas()
    {

        $consulta = ProcessosCaixa::with('processo')
            ->when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
                return
                    $query->WhereHas('processo', function ($query3) {
                        $query3->where('regional', auth()->user()->regional());
                    });
            })
            ->where('idCaixa', 1)
            ->where('explicacaoEntrada', 'Notificação visualizada pelo interessado.')
            ->whereNull('data_saida')
            ->count();
        return $consulta;
    }

    public static function contadorPareceresExternos($hoje5, $idCaixa, $tipo, $restritoRegional)
    {
        $hoje = Carbon::today();
        $hoje5m = Carbon::today()->subDays(5);
        if ($hoje5 == 5) {
            $hoje = Carbon::today()->subDays(5);
        }

        $consulta = ProcessosCaixa::with('processo')
            ->when($restritoRegional == true &&
                auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
                return
                    $query->WhereHas('processo', function ($query3) {
                        $query3->where('regional', auth()->user()->regional());
                    });
            })
            ->when($tipo == 'Normal', function ($query2) use ($hoje) {
                return
                    $query2->whereDate('data_limite', '>=', $hoje->format('Y-m-d'));
            })
            ->when($tipo == 'Vencido' && $hoje5 == 5, function ($query2) use ($hoje) {
                return
                    $query2->whereDate('data_limite', '<', $hoje->format('Y-m-d'));
            })
            ->when($tipo == 'Vencido' && $hoje5 != 5, function ($query2) use ($hoje, $hoje5m) {
                return
                    $query2->whereDate('data_limite', '<', $hoje->format('Y-m-d'))
                        ->whereDate('data_limite', '>=', $hoje5m->format('Y-m-d'));
            })
            ->where('idCaixa', $idCaixa)
            ->whereNull('data_saida')
            ->whereNotNull('data_limite')
            ->count();


        return $consulta;
    }

    public static function IndicadoresICA($idAnalistaTecnicoICA, $regional, $estado, $anoSelecionado, $dataTermino, $dataInicio, $situacaoICA)
    {


        $geral = ProcessosCaixa::with('processo')
            ->where('idCaixa', 9)
            ->when($anoSelecionado, function ($query) use ($anoSelecionado) {
                return
                    $query->whereYear('created_at', $anoSelecionado);
            })
            ->when($idAnalistaTecnicoICA, function ($query) use ($idAnalistaTecnicoICA) {
                return
                    $query->where('idUsuarioSaida', $idAnalistaTecnicoICA);
            })
            ->when($dataInicio && $dataTermino, function ($query) use ($dataTermino, $dataInicio) {
                return
                    $query->whereBetween('created_at', [$dataInicio, $dataTermino]);
            })
            ->when($dataInicio && !$dataTermino, function ($query2) use ($dataInicio) {
                return
                    $query2->whereDate('created_at', '>=', $dataInicio);
            })
            ->when(!$dataInicio && $dataTermino, function ($query2) use ($dataTermino) {
                return
                    $query2->whereDate('created_at', '<=', $dataTermino);
            })
            ->when($regional, function ($query) use ($regional) {
                return
                    $query->WhereHas('processo', function ($query3) use ($regional) {
                        $query3->where('regional', $regional);
                    });
            })
            ->when($estado, function ($query) use ($estado) {
                return
                    $query->WhereHas('processo', function ($query3) use ($estado) {
                        $query3->where('uf', $estado);
                    });
            })
            ->when(auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
                return
                    $query->WhereHas('processo', function ($query3) {
                        $query3->where('regional', auth()->user()->regional());
                    });
            })
            ->get(['id', 'id_subcaixa', 'data_limite', 'data_saida', 'idUsuarioSaida']);

        $subcaixas = Subcaixa::where('idCaixa', 9)->get(['id', 'nome']);

        $arraySubcaixas = [];
        $arraySubcaixasNoPrazo = [];
        $arraySubcaixasNoFora = [];
        $arraySubcaixasNoRespondidasNoPrazo = [];
        $arraySubcaixasNoRespondidasNoFora = [];

        foreach ($subcaixas as $subcaixa) {

            $arraySubcaixas[] = $geral->filter(function ($value, $key) use ($subcaixa) {
                return $value->id_subcaixa == $subcaixa->id;
            });


            $arraySubcaixasNoPrazo[] = $geral->filter(function ($value, $key) use ($subcaixa) {
                $foraPrazo = true;
                if ($value->data_limite) {
                    $dataLimite = Carbon::createFromFormat('Y-m-d', $value->data_limite);
                    if ($dataLimite->lessThan(Carbon::today())) {
                        $foraPrazo = false;
                    }
                }
                return $value->id_subcaixa == $subcaixa->id && $value->data_saida == null && !$foraPrazo;
            });
            $arraySubcaixasNoFora[] = $geral->filter(function ($value, $key) use ($subcaixa) {
                $foraPrazo = true;
                if ($value->data_limite) {
                    $dataLimite = Carbon::createFromFormat('Y-m-d', $value->data_limite);
                    if ($dataLimite->lessThan(Carbon::today())) {
                        $foraPrazo = false;
                    }
                }
                return $value->id_subcaixa == $subcaixa->id && $value->data_saida == null && $foraPrazo;
            });
            $arraySubcaixasNoRespondidasNoFora[] = $geral->filter(function ($value, $key) use ($subcaixa) {
                $foraPrazo = true;
                if ($value->data_limite && $value->data_saida) {
                    $dataLimite = Carbon::createFromFormat('Y-m-d', $value->data_limite);
                    $dataSaida = Carbon::createFromFormat('Y-m-d', $value->data_saida);
                    if ($dataLimite->lessThan($dataSaida)) {
                        $foraPrazo = false;
                    }
                }
                return $value->id_subcaixa == $subcaixa->id && $value->data_saida != null && !$foraPrazo;
            });
            $arraySubcaixasNoRespondidasNoPrazo[] = $geral->filter(function ($value, $key) use ($subcaixa) {
                $foraPrazo = true;
                if ($value->data_limite && $value->data_saida) {
                    $dataLimite = Carbon::createFromFormat('Y-m-d', $value->data_limite);
                    $dataSaida = Carbon::createFromFormat('Y-m-d', $value->data_saida);
                    if ($dataLimite->lessThan($dataSaida)) {
                        $foraPrazo = false;
                    }
                }
                return $value->id_subcaixa == $subcaixa->id && $value->data_saida != null && $foraPrazo;
            });
        }
        return [$arraySubcaixas, $arraySubcaixasNoPrazo, $arraySubcaixasNoFora, $arraySubcaixasNoRespondidasNoPrazo, $arraySubcaixasNoRespondidasNoFora, $subcaixas, $idAnalistaTecnicoICA, $regional, $estado, $anoSelecionado, $dataTermino, $dataInicio, $situacaoICA];
//        return ['falta' => $falta, 'respondido' => $respondido];
    }

    public static function OACO1_G()
    {
        $hoje5 = 0;
        $idCaixa = 1;
        $tipo = 'Normal';
        $restritoRegional = true;

        $hoje = Carbon::today();
        $hoje5m = Carbon::today()->subDays(5);
        if ($hoje5 == 5) {
            $hoje = Carbon::today()->subDays(5);
        }

        $consulta = ProcessosCaixa::with('processo')
            ->when($restritoRegional == true &&
                auth()->user()->hasAnyRole(['AGA', 'OACO', 'OAGA', 'Protoloco', 'ATM', 'DT']), function ($query) {
                return
                    $query->WhereHas('processo', function ($query3) {
                        $query3->where('regional', auth()->user()->regional());
                    });
            })
            ->where('idCaixa', $idCaixa)
            ->whereNull('data_saida')
            ->whereNull('data_limite')
            ->count();


        return $consulta;
    }

    public static function ATM_G()
    {
        return Contador::contadorPareceresExternos(0, 14, 'Normal', true);
    }

    public static function ATM_R()
    {
        return Contador::contadorPareceresExternos(0, 14, 'Vencido', true);
    }

    public static function ATM_P()
    {
        return Contador::contadorPareceresExternos(5, 14, 'Vencido', true);
    }

    public static function DT_G()
    {
        return Contador::contadorPareceresExternos(0, 13, 'Normal', true);
    }

    public static function DT_R()
    {
        return Contador::contadorPareceresExternos(0, 13, 'Vencido', true);
    }

    public static function DT_P()
    {
        return Contador::contadorPareceresExternos(5, 13, 'Vencido', true);
    }

    public static function ICA_G()
    {
        return Contador::contadorPareceresExternos(0, 9, 'Normal', false);
    }

    public static function ICA_R()
    {
        return Contador::contadorPareceresExternos(0, 9, 'Vencido', false);
    }

    public static function ICA_P()
    {
        return Contador::contadorPareceresExternos(5, 9, 'Vencido', false);
    }

    public static function CGNA_G()
    {
        return Contador::contadorPareceresExternos(0, 15, 'Normal', false);
    }

    public static function CGNA_R()
    {
        return Contador::contadorPareceresExternos(0, 15, 'Vencido', false);
    }

    public static function CGNA_P()
    {
        return Contador::contadorPareceresExternos(5, 15, 'Vencido', false);
    }

    public static function CENIPA_G()
    {
        return Contador::contadorPareceresExternos(0, 12, 'Normal', true);
    }

    public static function CENIPA_R()
    {
        return Contador::contadorPareceresExternos(0, 12, 'Vencido', true);
    }

    public static function CENIPA_P()
    {
        return Contador::contadorPareceresExternos(5, 12, 'Vencido', true);
    }

    public static function DECEA_G()
    {
        return Contador::contadorPareceresExternos(0, 11, 'Normal', false);
    }

    public static function DECEA_R()
    {
        return Contador::contadorPareceresExternos(0, 11, 'Vencido', false);
    }

    public static function DECEA_P()
    {
        return Contador::contadorPareceresExternos(5, 11, 'Vencido', false);
    }

    public static function JJAER_G()
    {
        return Contador::contadorPareceresExternos(0, 16, 'Normal', false);
    }

    public static function JJAER_R()
    {
        return Contador::contadorPareceresExternos(0, 16, 'Vencido', false);
    }

    public static function JJAER_P()
    {
        return Contador::contadorPareceresExternos(5, 16, 'Vencido', false);
    }

    public static function GABAER_G()
    {
        return Contador::contadorPareceresExternos(0, 19, 'Normal', false);
    }

    public static function GABAER_R()
    {
        return Contador::contadorPareceresExternos(0, 19, 'Vencido', false);
    }

    public static function GABAER_P()
    {
        return Contador::contadorPareceresExternos(5, 19, 'Vencido', false);
    }

    public static function COMGAP_G()
    {
        return Contador::contadorPareceresExternos(0, 17, 'Normal', false);
    }

    public static function COMGAP_R()
    {
        return Contador::contadorPareceresExternos(0, 17, 'Vencido', false);
    }

    public static function COMGAP_P()
    {
        return Contador::contadorPareceresExternos(5, 17, 'Vencido', false);
    }

    public static function COMPREP_G()
    {
        return Contador::contadorPareceresExternos(0, 18, 'Normal', false);
    }

    public static function COMPREP_R()
    {
        return Contador::contadorPareceresExternos(0, 18, 'Vencido', false);
    }

    public static function COMPREP_P()
    {
        return Contador::contadorPareceresExternos(5, 18, 'Vencido', false);
    }

    public static function Desenvolvedor()
    {
        $consulta = User::role('Desenvolvedor')
            ->count();
        return $consulta;
    }

    public static function Externo()
    {
        $consulta = User::role('Externo')
            ->count();
        return $consulta;
    }

    public static function Interno()
    {
        $consulta = User::role('Interno')
            ->count();
        return $consulta;
    }

    public static function AGA()
    {
        $consulta = User::role('AGA')
            ->count();
        return $consulta;
    }

    public static function OAGA()
    {
        $consulta = User::role('OAGA')
            ->count();
        return $consulta;
    }

    public static function OACO()
    {
        $consulta = User::role('OACO')
            ->count();
        return $consulta;
    }

    public static function ATM()
    {
        $consulta = User::role('ATM')
            ->count();
        return $consulta;
    }

    public static function SDOP()
    {
        $consulta = User::role('SDOP')
            ->count();
        return $consulta;
    }

    public static function ICA()
    {
        $consulta = User::role('ICA')
            ->count();
        return $consulta;
    }

    public static function Protocolo()
    {
        $consulta = User::role('Protocolo')
            ->count();
        return $consulta;
    }

    public static function DT()
    {
        $consulta = User::role('DT')
            ->count();
        return $consulta;
    }

    public static function CGNA()
    {
        $consulta = User::role('CGNA')
            ->count();
        return $consulta;
    }

    public static function CENIPA()
    {
        $consulta = User::role('CENIPA')
            ->count();
        return $consulta;
    }

    public static function JJAER()
    {
        $consulta = User::role('JJAER')
            ->count();
        return $consulta;
    }

    public static function AAL()
    {
        $consulta = User::role('AAL')
            ->count();
        return $consulta;
    }

    public static function Todos()
    {
        $consulta = User::count();
        return $consulta;
    }

}
