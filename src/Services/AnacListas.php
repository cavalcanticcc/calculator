<?php
namespace Atd\Calculator\Services;

class AnacListas
{

    public function status(): array
    {
        $status = [
            'Em preenchimento',
            'Enviado para análise',
            'Em análise',
            'Autorizado',
            'Não autorizado. Requerente necessita realizar correção.',
        ];
        return $status;
    }

    public function uf(): array
    {
        $uf = [
            'AC',
            'AL',
            'AP',
            'AM',
            'BA',
            'CE',
            'DF',
            'ES',
            'GO',
            'MA',
            'MT',
            'MS',
            'MG',
            'PA',
            'PB',
            'PR',
            'PE',
            'PI',
            'RJ',
            'RN',
            'RS',
            'RO',
            'RR',
            'SC',
            'SP',
            'SE',
            'TO',
        ];
        return $uf;
    }

    public function creaUf(): array
    {
        $creaUf = [
            'CREA-AC',
            'CREA-AL',
            'CREA-AP',
            'CREA-AM',
            'CREA-BA',
            'CREA-CE',
            'CREA-DF',
            'CREA-ES',
            'CREA-GO',
            'CREA-MA',
            'CREA-MT',
            'CREA-MS',
            'CREA-MG',
            'CREA-PA',
            'CREA-PB',
            'CREA-PR',
            'CREA-PE',
            'CREA-PI',
            'CREA-RJ',
            'CREA-RN',
            'CREA-RS',
            'CREA-RO',
            'CREA-RR',
            'CREA-SC',
            'CREA-SP',
            'CREA-SE',
            'CREA-TO',
        ];
        return $creaUf;
    }

}

