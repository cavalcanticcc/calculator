<?php

namespace Atd\Calculator\Services;

use League\Geotools\Coordinate\Ellipsoid;
use Toin0u\Geotools\Facade\Geotools;
use App\Utils\FormulasVincenty;
use App\Fiadpista;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class AnaliseAutomatica
{

    public function analiseSombra($resultado)
    {
        if (count($resultado->verticesopea) > 0 &&
            count($resultado->verticessombra) > 0) {
            $FormulasVincenty = new FormulasVincenty();
            $distancias = [];

            $minimo = 100000;
            $distanciaMenorVerticeOpeaMaisDistante = 100000;
            $maximoNoMinimo = -1;


            $posicaoMinimo = -1;
            $posicaoMaximo = -1;
            $posicao2SombraParaGerarReta = -1;
            $posicaoVerticeOpeaMaximaDistanciaDaReta = -1;

            $paraVerticeMaisDistanteOpeaPosicaoSombraMaisProximca = -1;
            $paraVerticeMaisDistanteOpeaDistanciaSombraMaisProximca = 100000;

            $distanciaCritica = [];
            $posicaoCritica = [];

            $distanciaMaximaDaReta = 0;

            foreach ($resultado->verticesopea as $keyOpea => $valueOpes) {
                $distancias[$keyOpea] = [];
                foreach ($resultado->verticessombra as $keySombra => $valueSombra) {
                    $distancias[$keyOpea][$keySombra] = round($FormulasVincenty->coordenadasDistancias(
                        $valueOpes['coordenadas']['latitude'], $valueOpes['coordenadas']['longitude'], $valueSombra['coordenadas']['latitude'], $valueSombra['coordenadas']['longitude'])[0], 1);
                    if ($distancias[$keyOpea][$keySombra] < $minimo) {
                        $minimo = $distancias[$keyOpea][$keySombra];
                        $posicaoMinimo = $keySombra;
                    }
                }
            }
            if ($posicaoMinimo !== null && $posicaoMinimo != -1 && isset($distancias[$posicaoMinimo])) {
                foreach ($distancias[$posicaoMinimo] as $KeyNoMinimo => $noMinimo) {
                    if ($noMinimo > $maximoNoMinimo) {
                        $maximoNoMinimo = $noMinimo;
                        $posicaoMaximo = $KeyNoMinimo;
                    }
                }

                if ($posicaoMaximo !== null && $posicaoMaximo != -1 && isset($distancias[$posicaoMaximo])) {
                    foreach ($distancias[$posicaoMaximo] as $KeyNoMaximoOPEA => $noMaximoOPEA) {
                        if ($distanciaMenorVerticeOpeaMaisDistante > $noMaximoOPEA) {
                            $distanciaMenorVerticeOpeaMaisDistante = $noMaximoOPEA;
                            $posicao2SombraParaGerarReta = $KeyNoMaximoOPEA;
                        }
                    }
                }
            }

            /*         se $posicao2SombraParaGerarReta == $posicaoMaximo então não precisa de Y e é usado na sombra
              se não foreach para achar os Y com reta entre pontos na sombra com posições
              $posicaoMinimo            $posicao2SombraParaGerarReta
             */

            $paraVerticeMaisDistanteOpeaPosicaoSombraMaisProximca = $posicaoMinimo;
            $decomposicao = [];
            if ($posicaoMinimo != $posicao2SombraParaGerarReta) {
                $p1 = null;
                if ($posicaoMinimo != -1) {
                    $p1 = $resultado->verticessombra[$posicaoMinimo];
                }
                $p2 = null;
                if ($posicao2SombraParaGerarReta != -1) {
                    $p2 = $resultado->verticessombra[$posicao2SombraParaGerarReta];
                }
                $Analise = new AnaliseAutomatica();


                $paraVerticeMaisDistanteOpeaPosicaoSombraMaisProximca = $posicao2SombraParaGerarReta;

                $paraVerticeMaisDistanteOpeaDistanciaSombraMaisProximca = null;
                if ($posicaoMaximo != -1 && $posicao2SombraParaGerarReta != -1) {
                    $paraVerticeMaisDistanteOpeaDistanciaSombraMaisProximca = $distancias[$posicaoMaximo][$posicao2SombraParaGerarReta];
                }
                foreach ($resultado->verticesopea as $keyOpea => $value) {
                    if ($p1 && $p2) {
                        $decomposicaoCadaPonto = $Analise->decomposicaoVetorial(
                            $p1->coordenadas['latitude'], $p1->coordenadas['longitude'], $p2->coordenadas['latitude'], $p2->coordenadas['longitude'], $value->coordenadas['latitude'], $value->coordenadas['longitude'], 1);

                        $decomposicao[] = $decomposicaoCadaPonto;
                        $posicaoCritica[] = $keyOpea;
                        if ($decomposicaoCadaPonto[3] < 0 && $decomposicaoCadaPonto[8] < 0) {
                            $distanciaCritica[] = round(abs($decomposicaoCadaPonto[10]), 1);
                        } else {
                            $distanciaCritica[] = round(min($decomposicaoCadaPonto[1][0], $decomposicaoCadaPonto[2][0]), 1);
                        }
                    }
                }
                if (count($distanciaCritica) > 0) {
                    $distanciaMaximaDaReta = max($distanciaCritica);
                    $posicaoVerticeOpeaMaximaDistanciaDaReta = array_search($distanciaMaximaDaReta, $distanciaCritica);
                }
                if ($posicaoVerticeOpeaMaximaDistanciaDaReta != -1) {
                    if (min($distancias[$posicaoVerticeOpeaMaximaDistanciaDaReta]) < $distanciaMaximaDaReta) {
                        $distanciaMaximaDaReta = min($distancias[$posicaoVerticeOpeaMaximaDistanciaDaReta]);
                    }
                }
            }


            $analiseOpea = [
                'posicaoVerticeOpeaMaximaDistanciaDaReta' => $posicaoVerticeOpeaMaximaDistanciaDaReta,
                'distanciaMaximaDaReta' => $distanciaMaximaDaReta,
                'minino' => $minimo,
                'maximoNoMinimo' => $maximoNoMinimo,
                'posicaoMinimo' => $posicaoMinimo,
                'posicaoMaximo' => $posicaoMaximo,
                'posicao2SombraParaGerarReta' => $posicao2SombraParaGerarReta,
                'paraVerticeMaisDistanteOpeaPosicaoSombraMaisProximca' => $paraVerticeMaisDistanteOpeaPosicaoSombraMaisProximca,
                'paraVerticeMaisDistanteOpeaDistanciaSombraMaisProximca' => $paraVerticeMaisDistanteOpeaDistanciaSombraMaisProximca,
                'decomposicao' => $decomposicao,
            ];

            //0   $d12
//1   $d13
//2   $d23
//3   $d13x
//4   $d13y
//5   $deslocandoXCab1
//6   $deslocandoYCab1
//7   $erroCab1[0]
//8   $d23x
//9   $d23y
//10  dy
//11  $erroCab2[0]
//12  $deslocandoXCab2
//13  $deslocandoYCab2
//            dd(compact('analiseOpea', 'distancias', 'p1', 'p2'));

            return ['analiseOpea' => $analiseOpea,
                'distancias' => $distancias];
        }
    }

    public function analiseAuxilios($aux, $gerenciada)
    {

        $superficie = '';
        $dt = '';
        $viola = '';

        if ($aux->base == null) {
            $dt = 'Sem dados';
            $resultadoAnalise = ['dt' => $dt,
                'superficie' => $superficie,
                'viola' => $viola];
            return $resultadoAnalise;
        }
        $topoObj = $gerenciada->altitudeBase + $gerenciada->altura;

        if ($aux->tipo == 'DVOR' || $aux->tipo == 'DME') {
            $resultadoAuxilioCircular = $this->auxilioCircular(100, 5100, 4, $aux->base, $topoObj, $aux->distancia);
        } else if ($aux->tipo == 'VOR') {
            $resultadoAuxilioCircular = $this->auxilioCircular(100, 15100, 2, $aux->base, $topoObj, $aux->distancia);
        } else if ($aux->tipo == 'VDB') {
            $resultadoAuxilioCircular = $this->auxilioCircular(100, 5100, 5, $aux->base, $topoObj, $aux->distancia);
        } else if ($aux->tipo == 'ESTACAO') {
            $resultadoAuxilioCircular = $this->auxilioCircular(50, 5050, 5, $aux->base, $topoObj, $aux->distancia);
        } else if ($aux->tipo == 'RADAR') {
            $resultadoAuxilioCircular = $this->auxilioCircular(100, 5100, 5, $aux->base, $topoObj, $aux->distancia);
        } else {
            $dt = 'Não Automatizado';
            $resultadoAnalise = ['dt' => $dt,
                'superficie' => $superficie,
                'viola' => $viola
            ];
            return $resultadoAnalise;
        }

        return $resultadoAuxilioCircular;
    }

    public function auxilioCircular($raioMenor, $raioMaior, $gradiente, $elevacaoAuxilio, $topoObjeto, $distancia)
    {

        $raioMenor = str_replace(',', '.', $raioMenor);
        $raioMaior = str_replace(',', '.', $raioMaior);
        $gradiente = str_replace(',', '.', $gradiente);
        $elevacaoAuxilio = str_replace(',', '.', $elevacaoAuxilio);
        $topoObjeto = str_replace(',', '.', $topoObjeto);
        $distancia = str_replace(',', '.', $distancia);


        $superficie = '';
        $dt = '';
        $viola = '';

        if ($distancia < $raioMaior) {
            if ($distancia < $raioMenor) {
                $superficie = 'Horizontal';
                $dt = 'Sim';
                if ($topoObjeto > $elevacaoAuxilio) {
                    $viola = number_format($topoObjeto - $elevacaoAuxilio, 2, ',', '');
                }
            } else {
                $superficie = 'Rampa';
                if ($distancia < 1000) {
                    $dt = 'Sim';
                }

//                dd($elevacaoAuxilio);
                if ($topoObjeto > $elevacaoAuxilio + (($distancia - $raioMenor) * $gradiente / 100)) {
                    $viola = number_format($topoObjeto - ($elevacaoAuxilio + (($distancia - $raioMenor) * $gradiente / 100)), 2, ',', '');
                    $dt = 'Sim';
                }
            }
        }

        $resultadoAnaliseCircular = ['dt' => $dt,
            'superficie' => $superficie,
            'viola' => $viola
        ];
        return $resultadoAnaliseCircular;
    }

    public function calculandoPerfil($perfilLongitudinal, $comprimentoPista, $comprimentoCadaEstaca)
    {
        $distanciaPerfil = [];
        $elevacaoPerfil = [];
        $contadorPedacosPerfil = 0;
        $ultimaDistancia = 0;
        if ($perfilLongitudinal != null &&
            $perfilLongitudinal != '' &&
            $perfilLongitudinal != 0 &&
            count($perfilLongitudinal) > 3) {
            $partindoPerfilLongitudinal = $perfilLongitudinal;
            foreach ($partindoPerfilLongitudinal as $index => $item) {
                $partindoPartes = $item;
//                $partindoPartes = explode(":", str_replace(',', '.', $item));
                if ($index != 0 &&
                    $comprimentoPista != null &&
                    $comprimentoPista != 0 &&
                    $partindoPartes[0] <= $comprimentoPista &&
                    $partindoPartes[0] > $ultimaDistancia) {
                    $distanciaPerfil[$contadorPedacosPerfil] = $partindoPartes[0];
                    $elevacaoPerfil[$contadorPedacosPerfil] = $partindoPartes[1];
                    $ultimaDistancia = $partindoPartes[0];
                    $contadorPedacosPerfil = $contadorPedacosPerfil + 1;
                }
            }
        }
        $perfilObjeto = ['$distanciaPerfil' => $distanciaPerfil,
            '$elevacaoPerfil' => $elevacaoPerfil];

        return $perfilObjeto;
    }

    public function analise($ad, $gerenciada)
    {


        $abertura1 = $this->recebeLadoRetornaAbertura($ad->decomposicaoVetorial[10], 1, $ad->e1c1AproximacaoDivergenciaLadoDireito, $ad->e2c1AproximacaoDivergenciaLadoEsquerdo, $ad->parametros, $ad->indices['aproximacao1']);

        $abertura2 = $this->recebeLadoRetornaAbertura($ad->decomposicaoVetorial[10], 2, $ad->e1c2AproximacaoDivergenciaLadoDireito, $ad->e2c2AproximacaoDivergenciaLadoEsquerdo, $ad->parametros, $ad->indices['aproximacao2']);

        $aproximacao1_1 = $this->aproximacao1($ad->indices['aproximacao1'], $ad->decomposicao, $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->parametros, $ad->c5CabeceiraMenorElevacao, $abertura1, 1);


        $decolagem1 = $this->decolagem($ad->indices['decolagem1'], $ad->decomposicao, $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->parametros, $ad->elevMaximaCab1FaixaPista, $ad->DecolagemCurvaCab1, 1, $ad->f5c1DecolagemLarguraFinal);

        $decolagem2 = $this->decolagem($ad->indices['decolagem2'], $ad->decomposicao, $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->parametros, $ad->elevMaximaCab2FaixaPista, $ad->DecolagemCurvaCab2, 2, $ad->f5c2DecolagemLarguraFinal);


        $aproximacao2_1 = $this->aproximacao1($ad->indices['aproximacao2'], $ad->decomposicao, $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->parametros, $ad->d5CabeceiraMaiorElevacao, $abertura2, 2);

        $aproximacao1_2 = [
            'Automatizado' => 'Sim',
            'Dentro' => 'Não',
            'Exigível' => 'Não',
            'Viola' => 'Não'
        ];
        $aproximacao1_H = [
            'Automatizado' => 'Sim',
            'Dentro' => 'Não',
            'Exigível' => 'Não',
            'Viola' => 'Não'
        ];
        $aproximacao2_2 = [
            'Automatizado' => 'Sim',
            'Dentro' => 'Não',
            'Exigível' => 'Não',
            'Viola' => 'Não'
        ];
        $aproximacao2_H = [
            'Automatizado' => 'Sim',
            'Dentro' => 'Não',
            'Exigível' => 'Não',
            'Viola' => 'Não'
        ];

        if ($aproximacao1_1['Dentro'] != 'Sim') {
            $aproximacao1_2 = $this->aproximacao2($ad->indices['aproximacao1'], $ad->decomposicao, $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->parametros, $ad->c5CabeceiraMenorElevacao, $abertura1, $ad->e16c1Aproximacao2Comprimento, 1);
        }
        if ($aproximacao1_1['Dentro'] != 'Sim' && $aproximacao1_2['Dentro'] != 'Sim') {
            $aproximacao1_H = $this->aproximacao3($ad->indices['aproximacao1'], $ad->decomposicao, $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->parametros, $ad->c5CabeceiraMenorElevacao, $abertura1, $ad->e16c1Aproximacao2Comprimento, 1);
        }
        if ($aproximacao2_1['Dentro'] != 'Sim') {
            $aproximacao2_2 = $this->aproximacao2($ad->indices['aproximacao2'], $ad->decomposicao, $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->parametros, $ad->d5CabeceiraMaiorElevacao, $abertura2, $ad->e16c2Aproximacao2Comprimento, 2);
        }


        if ($aproximacao2_1['Dentro'] != 'Sim' &&
            $aproximacao2_2['Dentro'] != 'Sim') {
            $aproximacao2_H = $this->aproximacao3($ad->indices['aproximacao2'], $ad->decomposicao, $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->parametros, $ad->d5CabeceiraMaiorElevacao, $abertura2, $ad->e16c2Aproximacao2Comprimento, 2);
        }
        $faixaPista = $this->faixaPista($ad, $gerenciada);
//         dd(unserialize($ad->cadaEstacaText));
//        $perfil = $this->calculandoPerfil(unserialize($ad->cadaEstacaText), $ad->b2Comprimento, $ad->comprimentoCadaEstaca);
//        $perfil = unserialize($ad->cadaEstacaText);
//        dd($perfil);
        $transicao = [
            'Automatizado' => 'Não',
            'Dentro' => 'Não',
            'Exigível' => 'Não',
            'Viola' => 'Não'
        ];

        if ($aproximacao1_1['Dentro'] != 'Sim' &&
            $aproximacao2_1['Dentro'] != 'Sim' &&
            $faixaPista['Dentro'] != 'Sim') {

            $transicao = $this->transicao($ad, $gerenciada);
        }

        $hi = $this->hi($ad, $gerenciada);
        $he = $this->he($ad, $gerenciada);

        $co = [
            'Automatizado' => 'Sim',
            'Dentro' => 'Não',
            'Exigível' => 'Não',
            'Viola' => 'Não'
        ];
        if ($hi['Dentro'] != 'Sim') {
            $co = $this->co($ad, $gerenciada);
        }


        $spvv1 = [
            'Automatizado' => 'Sim',
            'Dentro' => 'Não',
            'Exigível' => 'Não',
            'Viola' => 'Não'
        ];
        if ($ad->n1SPVV1Area == 1) {
            $dimensoes = $this->dimensoesSPVV(1, $ad->n3SPVV1Buffer, 0);
            $spvv1 = $this->spvv($dimensoes['comprimento'], $dimensoes['larguraD'], $dimensoes['larguraE'], $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->a5elevacao, $ad->decomposicao[3], $ad->decomposicao[8], $ad->decomposicao[10], $ad->n6c1SPVV1AltitudeAproximacaoDecolagem, $ad->n5c1SPVV1AltitudeTraves);
        }

        $spvv3 = [
            'Automatizado' => 'Sim',
            'Dentro' => 'Não',
            'Exigível' => 'Não',
            'Viola' => 'Não'
        ];
        if ($ad->n18SPVV3Area == 1) {
            $dimensoes = $this->dimensoesSPVV(3, $ad->n20SPVV3Buffer, 0);
            $spvv3 = $this->spvv($dimensoes['comprimento'], $dimensoes['larguraD'], $dimensoes['larguraE'], $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->a5elevacao, $ad->decomposicao[3], $ad->decomposicao[8], $ad->decomposicao[10], $ad->n23c1SPVV3AltitudeAproximacaoDecolagem, $ad->n22SPVV3AltitudeTraves);
        }

        $spvv2 = [
            'Automatizado' => 'Sim',
            'Dentro' => 'Não',
            'Exigível' => 'Não',
            'Viola' => 'Não'
        ];
        if ($ad->n9SPVV2Area == 1) {
            $dimensoes = $this->dimensoesSPVV(2, $ad->n12SPVV2Buffer, $ad->n10SPVV2CategoriaDesempenhoCritica);
            $spvv2 = $this->spvv($dimensoes['comprimento'], $dimensoes['larguraD'], $dimensoes['larguraE'], $gerenciada->altura, $gerenciada->altitudeBase + $gerenciada->altura, $ad->a5elevacao, $ad->decomposicao[3], $ad->decomposicao[8], $ad->decomposicao[10], $ad->n15c1SPVV2AltitudeAproximacaoDecolagem, $ad->n14c1SPVV2AltitudeTraves);
        }

        return [
            [
                'Decolagem C1' => $decolagem1,
                'Aproximação C1 S1' => $aproximacao1_1,
                'Aproximação C1 S2' => $aproximacao1_2,
                'Aproximação C1 SH' => $aproximacao1_H,
                'Decolagem C2' => $decolagem2,
                'Aproximação C2 S1' => $aproximacao2_1,
                'Aproximação C2 S2' => $aproximacao2_2,
                'Aproximação C2 SH' => $aproximacao2_H,
                'Transição' => $transicao,
                'Horizontal Interna' => $hi,
                'Cônica' => $co,
                'Horizontal Externa' => $he,
                'Faixa de Pista' => $faixaPista,
                'SPVV1' => $spvv1,
                'SPVV2' => $spvv2,
                'SPVV3' => $spvv3,
            ]
        ];
    }

    public function transicao($ad, $gerenciada)
    {

        $dentro = 'Não';
        $exigivel = 'Não';
        $explicacao = false;
        $viola = 'Não';

        $pontoTeste = new Point($gerenciada->latDecimal, $gerenciada->longDecimal);
        $checandoTransicao = Fiadpista::contains('transicao1', $pontoTeste)
            ->where('id', $ad->id)
            ->first();
        if ($checandoTransicao) {
            $dentro = 'Sim';
            $exigivel = 'Sim';
            $explicacao = 'Dentro dos limites da transição';
        }


        $retorno = [
            'Automatizado' => 'Sim',
            'Viola' => 'Não calculado',
            'Dentro' => $dentro,
            'Exigível' => $exigivel,
            'Explicação' => $explicacao
        ];

        return $retorno;
        //
//         $aerodromosComPista = Fiadpista::with('fiad', 'tentativa.processo')
//                    ->where('c3CabeceiraMenorLatitude', '<', $lataitudeMaxima)
//                    ->where('c3CabeceiraMenorLatitude', '>', $latitudeMinima)
//                    ->where('c4CabeceiraMenorlongitude', '<', $longitudeMaxima)
//                    ->where('c4CabeceiraMenorlongitude', '>', $longitudeMinima)
//                    ->WhereHas('fiad', function ($query3) {
//                        $query3->Where('emVigor', 1);
//                    })
//                    ->get();
//                foreach ($aerodromosComPista as $key => $ad) {
//                    $value->decomposicao = $Analise->decomposicaoVetorial(
//                        $ad->c3CabeceiraMenorLatitude,
//                        $ad->c4CabeceiraMenorLongitude,
//                        $ad->d3CabeceiraMaiorLatitude,
//                        $ad->d4CabeceiraMaiorLongitude,
//                        $gerenciada->latDecimal,
//                        $gerenciada->longDecimal,
//                        1);
//                    $value->analise = $Analise->analise($ad, $gerenciada);
//                }


    }

    public function aproximacao1($indice, $decomposicao, $altura, $topo, $parametros, $elevacaoCab, $abertura, $cabeceira)
    {

        $dentro = 'Não';
        $exigivel = 'Não';
        $viola = 'Não';
        $explicacao = false;
        $topoMaximo = false;

        $d1 = $decomposicao[1][0]; //1   d13
        $d2 = $decomposicao[2][0]; //2   d23
        $d1x = $decomposicao[3]; //3   d13x
        $d2x = $decomposicao[8]; //8   d23x
        $d1y = $decomposicao[10]; //10  dy

        if ($cabeceira == 1) {
            $dx = $d1x;
        } else {
            $dx = $d2x;
        }

        if ($dx > 0) {
            $deltaXOPEA = $dx - $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraDistanciaCabeceira'];
            $deltaYOPEA = $topo - $elevacaoCab;
            $elevacaoMaximaApr1 = $elevacaoCab + $deltaXOPEA * $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraGradiente'] / 100;

            if ($dx <= $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraComprimento'] +
                $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraDistanciaCabeceira'] &&
                $dx >= $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraDistanciaCabeceira'] &&
                abs($d1y) -
                ($parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraLarguraBordaInterna'] / 2) < (($dx -
                        $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraDistanciaCabeceira']) *
                    $abertura / 100)) {
                $exigivel = 'Sim';
                $dentro = 'Sim';
                $explicacao = ' Primeira Seção Aproximação ';
                if ($topo > $elevacaoMaximaApr1) {
                    $viola = number_format($topo - $elevacaoMaximaApr1, 2, ',', '');
                    $topoMaximo = number_format($elevacaoMaximaApr1, 2, ',', '');
                }
            }
        }


        $retorno = [
            'Automatizado' => 'Sim',
            'Dentro' => $dentro,
            'Máxima elevação permitida' => $topoMaximo,
            'Elevação Referencia' => $elevacaoCab,
            'Exigível' => $exigivel,
            'Viola' => $viola,
            'Explicação' => $explicacao
        ];

        return $retorno;
    }

    public function decolagem($indice, $decomposicao, $altura, $topo, $parametros, $elevacaoCab, $DecolagemCurvaCab, $cabeceira, $larguraFinal)
    {


        $dentro = 'Não';
        $exigivel = 'Não';
        $viola = 'Não';
        $explicacao = false;
        $topoMaximo = false;

        $d1 = $decomposicao[1][0]; //1   d13
        $d2 = $decomposicao[2][0]; //2   d23
        $d1x = $decomposicao[3]; //3   d13x
        $d2x = $decomposicao[8]; //8   d23x
        $d1y = $decomposicao[10]; //10  dy

        if ($cabeceira == 2) {
            $dx = $d1x;
        } else {
            $dx = $d2x;
        }

        if ($dx > 0) {
            $deltaXOPEA = $dx - $parametros['decolagem' . $cabeceira]['decolagemDistanciaCabeceira'];
            $deltaYOPEA = $topo - $elevacaoCab;
            $elevacaoMaxima = $elevacaoCab + $deltaXOPEA * $parametros['decolagem' . $cabeceira]['decolagemGradiente'] / 100;


            $semiLarguraFinalDecolagem = $larguraFinal / 2;

            $comprimentoParcialDecolagem = (($semiLarguraFinalDecolagem - $parametros['decolagem' . $cabeceira]['decolagemLarguraBordaInterna'])) * 100 /
                $parametros['decolagem' . $cabeceira]['decolagemAbertura'];

            if ($dx > $comprimentoParcialDecolagem +
                $parametros['decolagem' . $cabeceira]['decolagemDistanciaCabeceira'] &&
                $dx < $parametros['decolagem' . $cabeceira]['decolagemDistanciaCabeceira'] +
                $parametros['decolagem' . $cabeceira]['decolagemComprimento'] &&
                abs($d1y) < $semiLarguraFinalDecolagem) {

                $dentro = 'Sim';
                if ($dx < 3000 || ($dx >= 3000 && $topo - $elevacaoCab > 60)) {
                    $exigivel = 'Sim';
                }
                if ($topo > $elevacaoMaxima) {
                    $viola = number_format($topo - $elevacaoMaxima, 2, ',', '');
                    $topoMaximo = number_format($elevacaoMaxima, 2, ',', '');
                }
            } else if ($dx > $parametros['decolagem' . $cabeceira]['decolagemDistanciaCabeceira'] &&
                $dx < $parametros['decolagem' . $cabeceira]['decolagemDistanciaCabeceira'] +
                $parametros['decolagem' . $cabeceira]['decolagemComprimento'] &&
                abs($d1y) < $semiLarguraFinalDecolagem) {
                if (($dx - $parametros['decolagem' . $cabeceira]['decolagemDistanciaCabeceira'] >= (abs($d1y) -
                        $parametros['decolagem' . $cabeceira]['decolagemLarguraBordaInterna'] / 2) *
                    100 /
                    $parametros['decolagem' . $cabeceira]['decolagemAbertura'])) {
                    $dentro = 'Sim';
                    if ($dx < 3000 || ($dx >= 3000 && $topo - $elevacaoCab > 60)) {
                        $exigivel = 'Sim';
                    }
                    if ($topo > $elevacaoMaxima) {
                        $viola = number_format($topo - $elevacaoMaxima, 2, ',', '');
                        $topoMaximo = number_format($elevacaoMaxima, 2, ',', '');
                    }
                }
            }
        }


        $retorno = [
            'Automatizado' => 'Sim',
            'Dentro' => $dentro,
            'Máxima elevação permitida' => $topoMaximo,
            'Elevação Referencia' => $elevacaoCab,
            'Exigível' => $exigivel,
            'Viola' => $viola,
            'Explicação' => $explicacao
        ];

        return $retorno;
    }

    public function aproximacao2($indice, $decomposicao, $altura, $topo, $parametros, $elevacaoCab, $abertura, $comprimentoSegundaSecao, $cabeceira)
    {

        $dentro = 'Não';
        $exigivel = 'Não';
        $viola = 'Não';
        $explicacao = false;
        $topoMaximo = false;

        $d1 = $decomposicao[1][0]; //1   d13
        $d2 = $decomposicao[2][0]; //2   d23
        $d1x = $decomposicao[3]; //3   d13x
        $d2x = $decomposicao[8]; //8   d23x
        $d1y = $decomposicao[10]; //10  dy

        if ($cabeceira == 1) {
            $dx = $d1x;
        } else {
            $dx = $d2x;
        }

        if ($dx > 0) {
            $deltaXOPEA = $dx - $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraDistanciaCabeceira'];
            $deltaYOPEA = $topo - $elevacaoCab;
            $elevacaoMaximaApr1 = $elevacaoCab + $deltaXOPEA * $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraGradiente'] / 100;

            if ($dx <= $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraComprimento'] +
                $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraDistanciaCabeceira'] + $comprimentoSegundaSecao &&
                abs($d1y) < (($dx - $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraDistanciaCabeceira']) *
                    $abertura / 100) *
                1 + $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraLarguraBordaInterna'] / 2) {
                $dentro = 'Sim';
                $explicacao = ' Segunda Seção Aproximação ';
                if ($topo - $elevacaoCab > 60) {
                    $exigivel = 'Sim';
                }
                if ($parametros['aproximacao' . $cabeceira]['aproximacaoSegundaGradiente'] != '-') {

                    $elevacaoMaximaSegundaSecao = ($parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraComprimento'] *
                            $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraGradiente'] / 100) +
                        (($deltaXOPEA - $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraComprimento']) *
                            $parametros['aproximacao' . $cabeceira]['aproximacaoSegundaGradiente'] / 100) +
                        $elevacaoCab;
                    $topoMaximo = number_format($elevacaoMaximaSegundaSecao, 2, ',', '');

                    if ($topo > $elevacaoMaximaSegundaSecao) {
                        $viola = number_format($topo - $elevacaoMaximaSegundaSecao, 2, ',', '');
                    }
                } else {
                    $dentro = 'Não';
                    $exigivel = 'Não';
                    $viola = 'Não';
                    $explicacao = false;
                    $topoMaximo = false;
                }
            }
        }


        $retorno = [
            'Automatizado' => 'Sim',
            'Dentro' => $dentro,
            'Máxima elevação permitida' => $topoMaximo,
            'Elevação Referencia' => $elevacaoCab,
            'Exigível' => $exigivel,
            'Viola' => $viola,
            'Explicação' => $explicacao
        ];

        return $retorno;
    }

    public function aproximacao3($indice, $decomposicao, $altura, $topo, $parametros, $elevacaoCab, $abertura, $comprimentoSegundaSecao, $cabeceira)
    {

        $dentro = 'Não';
        $exigivel = 'Não';
        $viola = 'Não';
        $explicacao = false;
        $topoMaximo = false;

        $d1 = $decomposicao[1][0]; //1   d13
        $d2 = $decomposicao[2][0]; //2   d23
        $d1x = $decomposicao[3]; //3   d13x
        $d2x = $decomposicao[8]; //8   d23x
        $d1y = $decomposicao[10]; //10  dy

        if ($cabeceira == 1) {
            $dx = $d1x;
        } else {
            $dx = $d2x;
        }
//        dd($parametros);
        if ($parametros['aproximacao' . $cabeceira]['aproximacaoSegundaGradiente'] != '-' &&
            $parametros['aproximacao' . $cabeceira]['aproximacaoTerceiraGradiente'] != '-') {
            if ($dx > 0) {
                $deltaXOPEA = $dx - $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraDistanciaCabeceira'];
                $deltaYOPEA = $topo - $elevacaoCab;
                if ($comprimentoSegundaSecao != 12000) {
                    if ($dx <= 15060 &&
                        abs($d1y) < (($dx - $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraDistanciaCabeceira']) *
                            $abertura / 100) + $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraLarguraBordaInterna'] / 2) {
                        $dentro = 'Sim';
                        $explicacao = ' Seção Horizontal Aproximação';
                        $topoMaximo = number_format($elevacaoCab + 150, 2, ',', '');
                        if ($topo - $elevacaoCab > 140) {
                            $exigivel = 'Sim';
                        }
                        if ($topo - $elevacaoCab > 150) {
                            $viola = number_format($topo - $elevacaoCab - 150, 2, ',', '');
                        }
                    }
                }
            }
        }


        $retorno = [
            'Automatizado' => 'Sim',
            'Dentro' => $dentro,
            'Máxima elevação permitida' => $topoMaximo,
            'Elevação Referencia' => $elevacaoCab,
            'Exigível' => $exigivel,
            'Viola' => $viola,
            'Explicação' => $explicacao
        ];

        return $retorno;
    }

    public function recebeLadoRetornaAbertura($d1y, $cabeceira, $divergenciaD, $divergenciaE, $parametros, $indice)
    {

        if ($divergenciaD !== null && $divergenciaD >= 0) {
            $aberturaD = $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraAbertura'] +
                $divergenciaD;
        } else {
            $aberturaD = $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraAbertura'];
        }

        if ($divergenciaE != null && $divergenciaE >= 0) {
            $aberturaE = $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraAbertura'] +
                $divergenciaE;
        } else {
            $aberturaE = $parametros['aproximacao' . $cabeceira]['aproximacaoPrimeiraAbertura'];
        }
        if ($d1y >= 0 && $cabeceira == 1) {
            return $aberturaD;
        } else if ($d1y < 0 && $cabeceira == 1) {
            return $aberturaE;
        } else if ($d1y >= 0 && $cabeceira == 2) {
            return $aberturaE;
        } else if ($d1y < 0 && $cabeceira == 2) {
            return $aberturaD;
        }
    }

    public function co($ad, $gerenciada)
    {
        $dentro = 'Não';
        $exigivel = 'Não';
        $viola = 'Não';
        $explicacao = false;
        $topoMaximo = false;

        $indice = $ad->indices['geral'];
        $decomposicao = $ad->decomposicao;
        $altura = $gerenciada->altura;
        $topo = $gerenciada->altitudeBase + $gerenciada->altura;

        $adel = $ad->a5elevacao;

        $hiRaio = $ad->parametros['geral']['horizontalInternaRaio'];
        $coRaio = $ad->parametros['geral']['raioCO'];

        $d1 = $decomposicao[1][0];
        $d2 = $decomposicao[2][0];
        $d1x = $decomposicao[3];
        $d2x = $decomposicao[8];
        $d1y = $decomposicao[10];

        if (($d1 <= $coRaio && $d1 >= 0) ||
            ($d2 <= $coRaio && $d2 >= 0) ||
            ($d1x <= 0 && $d2x <= 0 && abs($d1y) <= $coRaio)) {
            $dentro = 'Sim';

            if ($d1x <= 0 && $d2x <= 0) {
                $distanciaPAnalise = abs($d1y);
            } else if ($d1 > $d2) {
                $distanciaPAnalise = abs($d2);
            } else {
                $distanciaPAnalise = abs($d1);
            }
            $topoMaximoCOConta = (($distanciaPAnalise - $hiRaio) * $ad->parametros['geral']['conicaGradiente'] / 100) + $adel + 45;
            $topoMaximo = number_format((($distanciaPAnalise - $hiRaio) * $ad->parametros['geral']['conicaGradiente'] / 100) + $adel + 45, 2, ',', '');

            if ($altura <= 19) {
                $explicacao = ' Co com Altura menor que 19m ';
            } else if ($topo - $adel <= 45) {
                $explicacao = ' CO com desnível menor que 45m ';
            } else {
                $exigivel = 'Sim';
                $explicacao = ' CO Exigível ';

                $violacaoCOConta = $topo - $topoMaximoCOConta;
                $viola = number_format($topo - $topoMaximoCOConta, 2, ',', '');
                if ($violacaoCOConta >= 0) {
                    $explicacao = ' CO ultrapassa os limites ';
                }
            }
        }

        $retorno = [
            'Automatizado' => 'Sim',
            'Dentro' => $dentro,
            'Máxima elevação permitida' => $topoMaximo,
            'Elevação Referencia' => $adel,
            'Exigível' => $exigivel,
            'Viola' => $viola,
            'Explicação' => $explicacao,
            'Raio' => $coRaio
        ];

        return $retorno;
    }

    public function hi($ad, $gerenciada)
    {
        $dentro = 'Não';
        $exigivel = 'Não';
        $viola = 'Não';
        $explicacao = false;
        $topoMaximo = false;

        $indice = $ad->indices['geral'];
        $decomposicao = $ad->decomposicao;
        $altura = $gerenciada->altura;
        $topo = $gerenciada->altitudeBase + $gerenciada->altura;

        $adel = $ad->a5elevacao;

        $hiRaio = $ad->parametros['geral']['horizontalInternaRaio'];

        $d1 = $decomposicao[1][0];
        $d2 = $decomposicao[2][0];
        $d1x = $decomposicao[3];
        $d2x = $decomposicao[8];
        $d1y = $decomposicao[10];

        if (($d1 <= $hiRaio && $d1 >= 0) ||
            ($d2 <= $hiRaio && $d2 >= 0) ||
            ($d1x <= 0 && $d2x <= 0 && abs($d1y) <= $hiRaio)) {
            $topoMaximo = number_format($adel + 45, 2, ',', '');
            $direfencaElevacaoOPEAAdel = $topo - $adel;
            $dentro = 'Sim';
            if ($altura <= 8) {
                $explicacao = ' HI com Altura menor que 8m ';
            } else if ($direfencaElevacaoOPEAAdel <= 40) {
                $explicacao = ' HI com desnível menor que 40m ';
            } else {
                $exigivel = 'Sim';
                $explicacao = ' HI Exigível ';
                if ($direfencaElevacaoOPEAAdel > 45) {
                    $viola = number_format($direfencaElevacaoOPEAAdel - 45, 2, ',', '');
                    $explicacao = ' HI ultrapassa os limites ';
                }
            }
        }

        $retorno = [
            'Automatizado' => 'Sim',
            'Dentro' => $dentro,
            'Máxima elevação permitida' => $topoMaximo,
            'Elevação Referencia' => $adel,
            'Exigível' => $exigivel,
            'Viola' => $viola,
            'Explicação' => $explicacao,
            'Raio' => $hiRaio
        ];

        return $retorno;
    }

    public function spvv($comprimento, $larguraD, $larguraE, $OPEAaltura, $OPEAtopo, $adel, $d1x, $d2x, $d1y, $altitudeAD, $altitudeT)
    {

        $dentro = 'Não';
        $exigivel = 'Não';
        $viola = 'Não';
        $explicacao = false;
        $topoMaximo = false;

        if ($d1x <= $comprimento && $d2x <= $comprimento) {
            if ($d1y >= 0) {
                $larguraUsada = $larguraD;
            } else {
                $larguraUsada = $larguraE;
            }
            if (abs($d1y) <= $larguraUsada) {
                $dentro = 'Sim';

                if ($OPEAaltura <= 30) {
                    $explicacao = 'altura <= 30';
                } else if ($OPEAtopo - $adel <= 55) {
                    $explicacao = 'topo-adel <= 55';
                } else {
                    $exigivel = 'Sim';
                    if (($d1x >= 0 || $d2x >= 0) && $OPEAtopo > $altitudeAD) {
                        $viola = number_format($OPEAtopo - $altitudeAD, 2, ',', '');
                        $topoMaximo = $altitudeAD;
                    } else if ($d2x < 0 && $d1x < 0 && $OPEAtopo > $altitudeT) {
                        $viola = number_format($OPEAtopo - $altitudeT, 2, ',', '');
                        $topoMaximo = $altitudeT;
                    }
                }
            }
        }

        $retorno = [
            'Automatizado' => 'Sim',
            'Dentro' => $dentro,
            'Máxima elevação permitida' => $topoMaximo,
            'Elevação Referencia' => $adel,
            'Exigível' => $exigivel,
            'Viola' => $viola,
            'Explicação' => $explicacao
        ];

        return $retorno;
    }

    public function dimensoesSPVV($area, $buffer, $categoria)
    {

        if ($area == 1) {
            if ($buffer == "0") {
                $larguraD = 2350;
                $larguraE = 2350;
            } else if ($buffer === "1") {
                $larguraD = 470;
                $larguraE = 2350;
            } else {
                $larguraD = 2350;
                $larguraE = 470;
            }
            $comprimento = 2350;
        } else if ($area == 2) {
            if ($categoria === "A") {
                if ($buffer == "0") {
                    $larguraD = 2350;
                    $larguraE = 2350;
                } else if ($buffer === "1") {
                    $larguraD = 470;
                    $larguraE = 2350;
                } else {
                    $larguraD = 2350;
                    $larguraE = 470;
                }
                $comprimento = 2780;
            } else if ($categoria === "B") {
                if ($buffer == "0") {
                    $larguraD = 2780;
                    $larguraE = 2780;
                } else if ($buffer === "1") {
                    $larguraD = 470;
                    $larguraE = 2780;
                } else {
                    $larguraD = 2780;
                    $larguraE = 470;
                }
                $comprimento = 2780;
            } else {
                if ($buffer == "0") {
                    $larguraD = 4170;
                    $larguraE = 4170;
                } else if ($buffer === "1") {
                    $larguraD = 930;
                    $larguraE = 4170;
                } else {
                    $larguraD = 4170;
                    $larguraE = 930;
                }
                $comprimento = 4170;
            }
        } else if ($area == 3) {
            if ($buffer == "0") {
                $larguraD = 7410;
                $larguraE = 7410;
            } else if ($buffer === "1") {
                $larguraD = 930;
                $larguraE = 7410;
            } else {
                $larguraD = 7410;
                $larguraE = 930;
            }
            $comprimento = 5560;
        }

        $dadosRetorno = ['larguraD' => $larguraD,
            'larguraE' => $larguraE,
            'comprimento' => $comprimento];

        return $dadosRetorno;
    }

    public function faixaPista($ad, $gerenciada)
    {
        $dentro = 'Não';
        $exigivel = 'Não';
        $viola = 'Não';
        $explicacao = false;

        $indice = $ad->indices['geral'];
        $decomposicao = $ad->decomposicao;
        $altura = $gerenciada->altura;
        $topo = $gerenciada->altitudeBase + $gerenciada->altura;

        $comprimentoZonaParada1 = 0;
        $comprimentoZonaParada2 = 0;
        if ($ad->c16CabeceiraMenorComprimentoZD) {
            $comprimentoZonaParada1 = $ad->c16CabeceiraMenorComprimentoZD;
        }
        if ($ad->d16CabeceiraMaiorComprimentoZD) {
            $comprimentoZonaParada2 = $ad->d16CabeceiraMaiorComprimentoZD;
        }
        $adel = $ad->a5elevacao;


        $d1 = $decomposicao[1][0];
        $d2 = $decomposicao[2][0];
        $d1x = $decomposicao[3];
        $d2x = $decomposicao[8];
        $d1y = $decomposicao[10];


        $ad->parametros['geral']['horizontalInternaRaio'];

        $limitex_aproximacao_cab1 = $ad->parametros['geral']['aproximacaoPrimeiraDistanciaCabeceira'] + $comprimentoZonaParada2;
        $limitex_aproximacao_cab2 = $ad->parametros['geral']['aproximacaoPrimeiraDistanciaCabeceira'] + $comprimentoZonaParada1;
        $limitey = $ad->parametros['geral']['aproximacaoPrimeiraLarguraBordaInterna'] / 2;


        if ($d1x <= $limitex_aproximacao_cab1 &&
            $d2x <= $limitex_aproximacao_cab2 &&
            abs($d1y) <= $limitey) {
            $dentro = 'Sim';
            $explicacao = ' Faixa de pista ';
            //dentro PF
        }

        $retorno = [
            'Automatizado' => 'Sim',
            'Dentro' => $dentro,
            'Exigível' => $exigivel,
            'Viola' => $viola,
            'Explicação' => $explicacao
        ];

        return $retorno;
    }

    public function he($ad, $gerenciada)
    {
        $dentro = 'Não';
        $exigivel = 'Não';
        $viola = 'Não';
        $explicacao = false;
        $topoMaximo = false;

        $indice = $ad->indices['geral'];
        $decomposicao = $ad->decomposicao;
        $altura = $gerenciada->altura;
        $topo = $gerenciada->altitudeBase + $gerenciada->altura;

        $adel = $ad->a5elevacao;

        $heRaio = 20000;

        $d1 = $decomposicao[1][0];
        $d2 = $decomposicao[2][0];
        $d1x = $decomposicao[3];
        $d2x = $decomposicao[8];
        $d1y = $decomposicao[10];

        if ($indice > 3) {
            if (($d1 <= $heRaio && $d1 >= 0) ||
                ($d2 <= $heRaio && $d2 >= 0) ||
                ($d1x <= 0 && $d2x <= 0 && abs($d1y) <= $heRaio)) {
                $topoMaximoHE = $adel + 150;
                $direfencaElevacaoOPEAAdel = $topo - $adel;
                $dentro = 'Sim';
                $topoMaximo = number_format($adel + 150, 2, ',', '');
                if ($altura <= 30) {
                    $explicacao = ' HE com Altura menor que 30m ';
                } else if ($direfencaElevacaoOPEAAdel <= 150) {
                    $explicacao = ' HE com desnível menor que 150m ';
                } else {
                    $exigivel = 'Sim';
                    $explicacao = ' HE Exigível ';
                    if ($direfencaElevacaoOPEAAdel > 150) {
                        $viola = number_format($direfencaElevacaoOPEAAdel - 150, 2, ',', '');
                        $explicacao = ' HE ultrapassa os limites ';
                    }
                }
            }
        }

        $retorno = [
            'Automatizado' => 'Sim',
            'Dentro' => $dentro,
            'Máxima elevação permitida' => $topoMaximo,
            'Elevação Referencia' => $adel,
            'Exigível' => $exigivel,
            'Viola' => $viola,
            'Explicação' => $explicacao
        ];

        return $retorno;
    }

    public function decomposicaoVetorial($lat1, $long1, $lat2, $long2, $lat3, $long3, $erroMaximo): array
    {

        $Formulas = new FormulasVincenty();


        $erro = [];
        $erro[0] = $erroMaximo + 1;
        $d13x = 0;
        $d13y = 0;
        $d23x = 0;
        $d23y = 0;

        $deslocandoX = 0;
        $lat1Inicio = $lat1;
        $long1Inicio = $long1;
        $lat2Inicio = $lat2;
        $long2Inicio = $long2;
        $d12 = $Formulas->coordenadasDistancias($lat1, $long1, $lat2, $long2); //0 distancia  2 rumo
        $d21 = $Formulas->coordenadasDistancias($lat2, $long2, $lat1, $long1); //0 distancia  2 rumo
        $d13 = $Formulas->coordenadasDistancias($lat3, $long3, $lat1, $long1);
        $distancia13 = abs($d13[0]);
        $d23 = $Formulas->coordenadasDistancias($lat3, $long3, $lat2, $long2);
        $distancia23 = abs($d23[0]);
        $rumoCab1 = $d12[2] + 180;
        if ($d13[2] < 0) {
            $d13[2] = $d13[2] + 180;
        }
        $rumoCab2 = $d21[2] + 180;
        if ($d23[2] < 0) {
            $d23[2] = $d23[2] + 180;
        }

        $d13x = $this->px($distancia13, $rumoCab1, $d13[2]);

        $d13y = $this->py($distancia13, $rumoCab1, $d13[2]);

        $d23x = $this->px($distancia23, $rumoCab2, $d23[2]);

        $d23y = $this->py($distancia23, $rumoCab2, $d23[2]);

        $d13x1 = $d13x;
        $d13y1 = $d13y;
        $d23x1 = $d23x;
        $d23y1 = $d23y;
        $rumoPistaCab1 = $d12[2];
        $rumoPistaCab2 = $d21[2];

        if ($rumoPistaCab1 >= 90) {
            $rumoPistaCab1Menos90 = $rumoPistaCab1 - 90;
        } else {
            $rumoPistaCab1Menos90 = $rumoPistaCab1 + 270;
        }

        if ($rumoPistaCab1 > 180) {
            $inversoRumoPistaCab1 = $rumoPistaCab1 - 180;
        } else {
            $inversoRumoPistaCab1 = $rumoPistaCab1 + 180;
        }


        if ($rumoPistaCab1Menos90 < 180) {
            $inversorumoPistaCab1Menos90 = $rumoPistaCab1Menos90 + 180;
        } else {
            $inversorumoPistaCab1Menos90 = $rumoPistaCab1Menos90 - 180;
        }

        if ($rumoPistaCab2 >= 90) {
            $rumoPistaCab2Menos90 = $rumoPistaCab2 - 90;
        } else {
            $rumoPistaCab2Menos90 = $rumoPistaCab2 + 270;
        }

        if ($rumoPistaCab2 > 180) {
            $inversoRumoPistaCab2 = $rumoPistaCab2 - 180; //aux5
        } else {
            $inversoRumoPistaCab2 = $rumoPistaCab2 + 180;
        }


        if ($rumoPistaCab2Menos90 < 180) {
            $inversorumoPistaCab2Menos90 = $rumoPistaCab2Menos90 + 180;
        } else {
            $inversorumoPistaCab2Menos90 = $rumoPistaCab2Menos90 - 180;
        }


        $deslocandoXCab1 = $Formulas->funcaoDiretaVicenty($lat1Inicio, $long1Inicio, $d13x, $inversoRumoPistaCab1);


        $deslocandoXCab2 = $Formulas->funcaoDiretaVicenty($lat2Inicio, $long2Inicio, $d23x, $inversoRumoPistaCab2);

        if ($d13y < 0) {
            $deslocandoYCab1 = $Formulas->funcaoDiretaVicenty($deslocandoXCab1[2], $deslocandoXCab1[3], abs($d13y), $rumoPistaCab1Menos90);

            $deslocandoYCab2 = $Formulas->funcaoDiretaVicenty($deslocandoXCab2[2], $deslocandoXCab2[3], abs($d23y), $inversorumoPistaCab2Menos90);
        } else {
            $deslocandoYCab1 = $Formulas->funcaoDiretaVicenty($deslocandoXCab1[2], $deslocandoXCab1[3], abs($d13y), $inversorumoPistaCab1Menos90);

            $deslocandoYCab2 = $Formulas->funcaoDiretaVicenty($deslocandoXCab2[2], $deslocandoXCab2[3], abs($d23y), $rumoPistaCab2Menos90);
        }


        $erroCab1 = $Formulas->coordenadasDistancias($lat3, $long3, $deslocandoYCab1[2], $deslocandoYCab1[3]);
        $erroCab2 = $Formulas->coordenadasDistancias($lat3, $long3, $deslocandoYCab2[2], $deslocandoYCab2[3]);
        $deslocandoY1Cab1 = $deslocandoYCab1;
        $deslocandoY1Cab2 = $deslocandoYCab2;
        $erro1Cab1 = $erroCab1;
        $erro1Cab2 = $erroCab2;
        $d13x1 = $d13x;
        $d13y1 = $d13y;
        if ($erroCab1[0] > $erroMaximo / 5 || $erroCab2[0] > $erroMaximo / 5 || true) {

            $distancia13Parcial = abs($erroCab1[0]);
            $distancia23Parcial = abs($erroCab2[0]);

            $d13xParcial = $this->px($distancia13Parcial, $rumoCab1, $erroCab1[2]);
            $d23xParcial = $this->px($distancia23Parcial, $rumoCab2, $erroCab2[2]);

            $d13yParcial = $this->py($distancia13Parcial, $rumoCab1, $erroCab1[2]);
            $d23yParcial = $this->py($distancia23Parcial, $rumoCab2, $erroCab2[2]);


            $d13y = $d13y + $d13yParcial;

            $d23y = $d23y + $d23yParcial;

            $d13x = $d13x + $d13xParcial;

            $d23x = $d23x + $d23xParcial;

            $deslocandoXCab1 = $Formulas->funcaoDiretaVicenty($lat1Inicio, $long1Inicio, $d13x, $inversoRumoPistaCab1);


            $deslocandoXCab2 = $Formulas->funcaoDiretaVicenty($lat2Inicio, $long2Inicio, $d23x, $inversoRumoPistaCab2);

            if ($d13y < 0) {
                $deslocandoYCab1 = $Formulas->funcaoDiretaVicenty($deslocandoXCab1[2], $deslocandoXCab1[3], abs($d13y), $rumoPistaCab1Menos90);

                $deslocandoYCab2 = $Formulas->funcaoDiretaVicenty($deslocandoXCab2[2], $deslocandoXCab2[3], abs($d23y), $inversorumoPistaCab2Menos90);
            } else {
                $deslocandoYCab1 = $Formulas->funcaoDiretaVicenty($deslocandoXCab1[2], $deslocandoXCab1[3], abs($d13y), $inversorumoPistaCab1Menos90);

                $deslocandoYCab2 = $Formulas->funcaoDiretaVicenty($deslocandoXCab2[2], $deslocandoXCab2[3], abs($d23y), $rumoPistaCab2Menos90);
            }

            $erroCab1 = $Formulas->coordenadasDistancias($lat3, $long3, $deslocandoYCab1[2], $deslocandoYCab1[3]);
            $erroCab2 = $Formulas->coordenadasDistancias($lat3, $long3, $deslocandoYCab2[2], $deslocandoYCab2[3]);
            $deslocandoY2Cab1 = $deslocandoYCab1;
            $deslocandoY2Cab2 = $deslocandoYCab2;
            $erro2Cab1 = $erroCab1;
            $erro2Cab2 = $erroCab2;
            $d13x2 = $d13x;
            $d13y2 = $d13y;
            $d23x2 = $d23x;
            $d23y2 = $d23y;
        }


        if ($erroCab1[0] > $erroMaximo / 5 || $erroCab2[0] > $erroMaximo / 5 || true) {

            $distancia13Parcial = abs($erroCab1[0]);
            $distancia23Parcial = abs($erroCab2[0]);

            $d13xParcial = $this->px($distancia13Parcial, $rumoCab1, $erroCab1[2]);
            $d23xParcial = $this->px($distancia23Parcial, $rumoCab2, $erroCab2[2]);

            $d13yParcial = $this->py($distancia13Parcial, $rumoCab1, $erroCab1[2]);
            $d23yParcial = $this->py($distancia23Parcial, $rumoCab2, $erroCab2[2]);


            $d13y = $d13y + $d13yParcial;

            $d23y = $d23y + $d23yParcial;

            $d13x = $d13x + $d13xParcial;

            $d23x = $d23x + $d23xParcial;

            $deslocandoXCab1 = $Formulas->funcaoDiretaVicenty($lat1Inicio, $long1Inicio, $d13x, $inversoRumoPistaCab1);


            $deslocandoXCab2 = $Formulas->funcaoDiretaVicenty($lat2Inicio, $long2Inicio, $d23x, $inversoRumoPistaCab2);

            if ($d13y < 0) {
                $deslocandoYCab1 = $Formulas->funcaoDiretaVicenty($deslocandoXCab1[2], $deslocandoXCab1[3], abs($d13y), $rumoPistaCab1Menos90);

                $deslocandoYCab2 = $Formulas->funcaoDiretaVicenty($deslocandoXCab2[2], $deslocandoXCab2[3], abs($d23y), $inversorumoPistaCab2Menos90);
            } else {
                $deslocandoYCab1 = $Formulas->funcaoDiretaVicenty($deslocandoXCab1[2], $deslocandoXCab1[3], abs($d13y), $inversorumoPistaCab1Menos90);

                $deslocandoYCab2 = $Formulas->funcaoDiretaVicenty($deslocandoXCab2[2], $deslocandoXCab2[3], abs($d23y), $rumoPistaCab2Menos90);
            }

            $erroCab1 = $Formulas->coordenadasDistancias($lat3, $long3, $deslocandoYCab1[2], $deslocandoYCab1[3]);
            $erroCab2 = $Formulas->coordenadasDistancias($lat3, $long3, $deslocandoYCab2[2], $deslocandoYCab2[3]);
            $deslocandoY3Cab1 = $deslocandoYCab1;
            $deslocandoY3Cab2 = $deslocandoYCab2;
            $erro3Cab1 = $erroCab1;
            $erro3Cab2 = $erroCab2;
            $d13x3 = $d13x;
            $d13y3 = $d13y;
            $d23x3 = $d23x;
            $d23y3 = $d23y;
        }

        if ($erro2Cab1[0] > $erro1Cab1[0] && $erro1Cab1[0] != null) {
            $erro2Cab1 = $erro1Cab1;
            $deslocandoY2Cab1 = $deslocandoY1Cab1;
            $d13x2 = $d13x1;
            $d13y2 = $d13y1;
        } else {
            $erro1Cab1 = $erro2Cab1;
            $deslocandoY1Cab1 = $deslocandoY2Cab1;
            $d13x1 = $d13x2;
            $d13y1 = $d13y2;
        }
        if ($erro2Cab2[0] > $erro1Cab2[0] && $erro1Cab2[0] != null) {
            $erro2Cab2 = $erro1Cab2;
            $deslocandoY2Cab2 = $deslocandoY1Cab2;
            $d23x2 = $d23x1;
            $d23y2 = $d23y1;
        } else {
            $erro1Cab2 = $erro2Cab2;
            $deslocandoY1Cab2 = $deslocandoY2Cab2;
            $d23x1 = $d23x2;
            $d23y1 = $d23y2;
        }
//
        if ($erro2Cab1[0] < $erro3Cab1[0] && $erro2Cab1[0] != null) {
            $erroCab1 = $erro2Cab1;
            $deslocandoYCab1 = $deslocandoY2Cab1;
            $d13x = $d13x2;
            $d13y = $d13y2;
        } else if ($erro1Cab1[0] < $erro3Cab1[0] && $erro1Cab1[0] != null) {
            $erroCab1 = $erro1Cab1;
            $deslocandoYCab1 = $deslocandoY1Cab1;
            $d13x = $d13x1;
            $d13y = $d13y1;
        } else if ($erro3Cab1[0] == '0') {
            $deslocandoYCab1 = $deslocandoY2Cab1;
            $d13x = $d13x2;
            $d13y = $d13y2;
            $erroCab1 = $erro2Cab1;
        }
        if ($erro2Cab2[0] < $erro3Cab2[0] && $erro2Cab2[0] != null) {
            $erroCab2 = $erro2Cab2;
            $deslocandoYCab2 = $deslocandoY2Cab2;
            $d23x = $d23x2;
            $d23y = $d23y2;
        } else if ($erro1Cab2[0] < $erro3Cab2[0] && $erro1Cab2[0] != null) {
            $erroCab2 = $erro1Cab2;
            $deslocandoYCab2 = $deslocandoY1Cab2;
            $d23x = $d23x1;
            $d23y = $d23y1;
        } else if ($erro3Cab2[0] == '0') {
            $deslocandoYCab2 = $deslocandoY2Cab2;
            $d23x = $d23x2;
            $d23y = $d23y2;
            $erroCab2 = $erro2Cab2;
        }

        if ($d13x > 0) {
            $dy = $d13y;
        } else if ($d23x > 0) {
            $dy = $d23y;
        } else {
            $dy = ((abs($d13x) * (abs($d23y) - abs($d13y)) / abs($d12[0])) + abs($d13y));
        }


//0   $d12
//1   $d13
//2   $d23
//3   $d13x
//4   $d13y
//5   $deslocandoXCab1
//6   $deslocandoYCab1
//7   $erroCab1[0]
//8   $d23x
//9   $d23y
//10  dy
//11  $erroCab2[0]
//12  $deslocandoXCab2
//13  $deslocandoYCab2

        return [$d12,
            $d13,
            $d23,
            $d13x,
            $d13y,
            $deslocandoXCab1,
            $deslocandoYCab1,
            $erroCab1[0],
            $d23x,
            $d23y,
            $dy,
            $erroCab2[0],
            $deslocandoXCab2,
            $deslocandoYCab2];
    }

    public function px($distancia, $az_app, $az_ob)
    {
        if ($az_app > 180) {
            $aux3 = $az_app - 180;
        } else {
            $aux3 = $az_app + 180;
        }
        $aux1 = $aux3 - $az_ob;
        if ($aux1 < 0) {
            $aux2 = $aux1 + 360;
        } else {
            $aux2 = $aux1;
        }

        if (($aux2 <= 90) || ($aux2 >= 270)) {
            if ($aux2 <= 90) {
                $px = cos($aux2 * pi() / 180) * $distancia;
            } else {
                $px = cos((360 - $aux2) * pi() / 180) * $distancia;
            }
        } else {
            if ($aux2 <= 180) {
                $px = cos((180 - $aux2) * pi() / 180) * $distancia * (-1);
            } else {
                if ($aux2 <= 270) {
                    $px = cos(($aux2 - 180) * pi() / 180) * $distancia * (-1);
                }
            }
        }
        return $px;
    }

    function py($distancia, $az_app, $az_ob)
    {

        if ($az_app > 180) {
            $aux3 = $az_app - 180;
        } else {
            $aux3 = $az_app + 180;
        }

        $aux1 = $aux3 - $az_ob;
        if ($aux1 < 0) {
            $aux2 = $aux1 + 360;
        } else {
            $aux2 = $aux1;
        }

        if (($aux2 <= 180)) {
            if ($aux2 <= 90) {
                $py = sin(pi() / 180 * ($aux2)) * $distancia;
            } else {
                $py = sin(pi() / 180 * (180 - $aux2)) * $distancia;
            }
        } else {
            if ($aux2 <= 270) {
                $py = sin(pi() / 180 * ($aux2 - 180)) * $distancia * (-1);
            } else {
                $py = sin(pi() / 180 * (360 - $aux2)) * $distancia * (-1);
            }
        }
        return $py;
    }

}
