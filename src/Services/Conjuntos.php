<?php

namespace Atd\Calculator\Services;


class Conjuntos
{

    public function cadaSuperficiePBZPAStyleKML(): array
    {
        $cadaSuperficiePBZPAStyleKML = [
            'HI' => ['nome' => 'Horizontal Interna',
                'style' => 'style_HI',
                'sigla' => 'HI',
                'tipoLegenda' => 'geral',
                'legendas' => [
                    'geral' => ['horizontalInternaAltura' => 'Altura',
                        'horizontalInternaRaio' => 'Raio',
                        'altitudeHI' => 'Altitude'
                    ]
                ]
            ],
            'CO' => ['nome' => 'Cônica',
                'style' => 'style_CO',
                'sigla' => 'CO',
                'tipoLegenda' => 'geral',
                'legendas' => [
                    'geral' => ['conicaGradiente' => 'Gradiente',
                        'conicaAltura' => 'Altura',
                        'altitudeHI' => 'Altitude interna',
                        'altitudeCO' => 'Altitude externa',
                        'raioCO' => 'Raio',
                    ]
                ]
            ],
            'Pista' => ['nome' => 'Pista',
                'style' => 'style_CO',
                'sigla' => 'Pista',
                'tipoLegenda' => 'geral',
                'legendas' => [
                    'geral' => [
                    ]
                ]
            ]
        ];


        return $cadaSuperficiePBZPAStyleKML;
    }

    public function tipoImplantacao(): array
    {
        $tipoImplantacao = array("Edificação",
            "Conjunto Habitacional",
            "Loteamento",
            "Parque Eólico",
            "Ponte",
            "Viaduto",
            "Aterro Sanitário",
            "Balão Cativo",
            "Posto de combustível",
            "Antena",
            "Torre",
            "Mastro",
            "Linha de Transmissão",
            "Projetor de Raio Laser",
            "Outros", 0);


        return $tipoImplantacao;
    }

}
