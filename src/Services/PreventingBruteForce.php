<?php

namespace Atd\Calculator\Services;

use App\Userlog;
use Illuminate\Support\Facades\Auth;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PreventingBruteForce
{
    static public function generico($banco, $threshold, $tempo, $user_id, $valor_id_user, $data)
    {
        $retorno = false;
        $period = Carbon::now()->subMinutes($tempo);
        $count = DB::table($banco)
            ->where($data, '>', $period)
            ->whereNotNull($user_id)
            ->where($user_id, $valor_id_user)
            ->count();
        if ($count > $threshold) {
            $roles = auth()->user()->roles;

            auth()->user()->syncRoles([]);
            $texto = "Bloqueio devido excesso de criação de linhas no banco.";
            $texto .= 'Banco: ' . $banco;
            $texto .= '. Registro no banco: ' . $threshold;
            $texto .= '. Intervalo de tempo dos registros: ' . $tempo . '.';

            if (count($roles) > 0) {
                $texto .= 'Permissões removidas:';
                foreach ($roles as $keyRole => $role) {
                    $texto .= $role->name . '  ';
                    if (count($roles) - 1 != $keyRole) {
                        $texto .= ' ,';
                    }
                }
                $texto .= ' .';
            }

            UserLog::guestLog($texto, auth()->user()->id);

            $desenvolvedores = [
                'cleydsoncavalcanti@gmail.com',
                'miguelgarciamgm@gmail.com',
                'alisonsouza@yahoo.com.br',
            ];

            foreach ($desenvolvedores as $desenvolvedor) {

                $mensagem = 'Tentativa de BruteForce. Usuário:' . auth()->user()->id;
                $mensagem .= '. Nome: ' . auth()->user()->name;
                $mensagem .= '. Email: ' . auth()->user()->email;
                $mensagem .= '. CPF: ' . auth()->user()->cpf;

                app('App\Http\Controllers\ConvidadoController')
                    ->enviarEmail($desenvolvedor, $mensagem, 'BruteForce SysAGA');
            }

            $retorno = true;
        }
        return $retorno;
    }

    static public function semLogar($banco, $threshold, $tempo, $user_id, $valor_id_user, $data)
    {
        $period = Carbon::now()->subMinutes($tempo);
        $count = DB::table($banco)
            ->where($data, '>', $period)
            ->whereNotNull($user_id)
            ->where($user_id, $valor_id_user)
            ->count();
        if ($count > $threshold) {
            $suspeito= DB::table($banco)
                ->whereNotNull($user_id)
                ->where($user_id, $valor_id_user)
                ->orderBy('id','desc')
                ->first();

            $texto = 'Tentativa de criar usuários em massa com mesmo ip.'.$valor_id_user;

            $idSuspeito=$suspeito->idGerenciado ? $suspeito->idGerenciado : $suspeito->idGerente;

            UserLog::guestLog($texto, $idSuspeito);

            $desenvolvedores = [
                'cleydsoncavalcanti@gmail.com',
                'miguelgarciamgm@gmail.com',
                'alisonsouza@yahoo.com.br',
            ];

            foreach ($desenvolvedores as $desenvolvedor) {
                $mensagem = 'Tentativa de BruteForce. Usuário:' . $suspeito->idGerente;
                app('App\Http\Controllers\ConvidadoController')
                    ->enviarEmail($desenvolvedor, $mensagem, 'BruteForce new user SysAGA');
            }
            dd('Erro ao tentar acessar, entre em contato com o DECEA através do SAC.');
        }
    }
}

