<?php

namespace Atd\Calculator\Services;

use Toin0u\Geotools\Facade\Geotools;
use App\Utils\FormulasVincenty;

class Desenhos
{

    public function circulo(string $latitude, string $longitude, string $raio, string $altitude, int $resolucao = 36): array
    {
        $retorno = [];

        $coordenada = Geotools::coordinate([$latitude, $longitude]);
        for ($i = 0; $i < $resolucao; $i++) {
            $destinationPoint = Geotools::Vertex()->setFrom($coordenada)->destination($i * (360 / $resolucao), $raio);

            $pontoCirculo = ['latitude' => $destinationPoint->getLatitude(),
                'longitude' => $destinationPoint->getLongitude(),
                'altitude' => $altitude
            ];
            $retorno[] = $pontoCirculo;
        }
        $retorno[] = $retorno[0];
        return $retorno;
    }

    public function elipse($lat1, $lng1, $lat2, $lng2, $raio = 4000, $altitude = 0, int $resolucao = 36): array
    {
        $retorno = [];
        $Vincenty = new FormulasVincenty;
        $rumo1 = $Vincenty->finalBearing($lat1, $lng1, $lat2, $lng2);
        $rumo2 = $Vincenty->finalBearing($lat2, $lng2, $lat1, $lng1);

        $anguloJaUsando = 180 * 1;
        for ($k = 0; $k <= 360; $k++) {
            if ($k <= 180) {

                $rumoUsado = $rumo1 + 90 + $k;

                $coordenada = Geotools::coordinate([$lat1, $lng1]);
                $destinationPoint = Geotools::Vertex()->setFrom($coordenada)->destination($rumoUsado, $raio);
                $pontoCirculo = ['latitude' => $destinationPoint->getLatitude(),
                    'longitude' => $destinationPoint->getLongitude(),
                    'altitude' => $altitude
                ];
                $retorno[] = $pontoCirculo;
            } else {
                $rumoUsado = $rumo2 - 90 + $k;
                $coordenada = Geotools::coordinate([$lat2, $lng2]);
                $destinationPoint = Geotools::Vertex()->setFrom($coordenada)->destination($rumoUsado, $raio);

                $pontoCirculo = ['latitude' => $destinationPoint->getLatitude(),
                    'longitude' => $destinationPoint->getLongitude(),
                    'altitude' => $altitude
                ];
                $retorno[] = $pontoCirculo;
            }
        }

        $retorno[] = $retorno[0];
        return $retorno;

        $coordenada = Geotools::coordinate([$latitude, $longitude]);
        for ($i = 0; $i < $resolucao; $i++) {
            $destinationPoint = Geotools::Vertex()->setFrom($coordenada)->destination($i * (360 / $resolucao), $raio);

            $pontoCirculo = ['latitude' => $destinationPoint->getLatitude(),
                'longitude' => $destinationPoint->getLongitude(),
                'altitude' => $altitude
            ];
            $retorno[] = $pontoCirculo;
        }
        $retorno[] = $retorno[0];
        return $retorno;
    }

    public function pista($lat1, $long1, $lat2, $long2, $largura): array
    {
        $Vincenty = new FormulasVincenty();

        $coordenadaAntes1 = [$lat1, $long1];
        $coordenada1 = Geotools::coordinate($coordenadaAntes1);

        $coordenadaAntes2 = [$lat2, $long2];
        $coordenada2 = Geotools::coordinate($coordenadaAntes2);

        $distance = Geotools::distance()->setFrom($coordenada1)->setTo($coordenada2);
        $comprimentoPista = $distance->in('m')->vincenty();
        $caminho = [];

        $temp = [];

        $temp['tipo'] = 'ponto';
        $temp['lat'] = $lat1;
        $temp['long'] = $long1;
        $temp['desenhar'] = true;
        $temp['altitude'] = 0;
        $caminho[] = $temp;

        $rumo = $Vincenty->finalBearing($lat1, $long1, $lat2, $long2);
        $temp['tipo'] = 'rumo';
        $temp['rumo'] = $rumo + 90;
        $temp['distancia'] = $largura / 2;
        $temp['desenhar'] = true;
        $temp['curva'] = true;
        $temp['altitude'] = 0;
        $caminho[] = $temp;

        $temp['tipo'] = 'rumo';
        $temp['rumo'] = 90;
        $temp['distancia'] = $comprimentoPista;
        $temp['desenhar'] = true;
        $temp['curva'] = true;
        $temp['altitude'] = 0;
        $caminho[] = $temp;

        $temp['tipo'] = 'ponto';
        $temp['lat'] = $lat2;
        $temp['long'] = $long2;
        $temp['desenhar'] = true;
        $temp['curva'] = true;
        $temp['altitude'] = 0;
        $caminho[] = $temp;

        $temp['tipo'] = 'rumo';
        $temp['rumo'] = 0;
        $temp['distancia'] = $largura / 2;
        $temp['desenhar'] = true;
        $temp['curva'] = true;
        $temp['altitude'] = 0;
        $caminho[] = $temp;

        $temp['tipo'] = 'rumo';
        $temp['rumo'] = $rumo + 180;
        $temp['distancia'] = $comprimentoPista;
        $temp['desenhar'] = true;
        $temp['curva'] = false;
        $temp['altitude'] = 0;
        $caminho[] = $temp;

        $temp['tipo'] = 'ponto';
        $temp['lat'] = $lat1;
        $temp['long'] = $long1;
        $temp['desenhar'] = true;
        $temp['altitude'] = 0;
        $caminho[] = $temp;


        $caminhando = $this->rumoDistancia($caminho);

        $retorno = [];

        return $caminhando;
    }

    public function rumoDistancia(array $caminho): array
    {

        $Vincenty = new FormulasVincenty();

        $retorno = [];
        $cadaLatitude = [];
        $cadaLongitude = [];
        $latitudePontoAnterior = $caminho[0]['lat'];
        $longitudePontoAnterior = $caminho[0]['long'];
        $rumoPontoAnterior = 0;

//                    dd(compact('rumoPontoAnterior','keyproximo'));

        foreach ($caminho as $keyproximo => $proximo) {
            if ($proximo['tipo'] != 'rumo') {
                $rumoPontoAnterior = $Vincenty->finalBearing($latitudePontoAnterior, $longitudePontoAnterior, $proximo['lat'], $proximo['long']);
                if ($proximo['desenhar'] == true) {
                    $pontoAuxiliar = [
                        'latitude' => $proximo['lat'],
                        'longitude' => $proximo['long'],
                        'altitude' => $proximo['altitude']
                    ];
                    $retorno[] = $pontoAuxiliar;
                }
            } else {
                if ($proximo['curva']) {
                    $somaRumos = (float)$rumoPontoAnterior + (float)$proximo['rumo'];
                    $rumoPontoAnterior = (float)$rumoPontoAnterior + (float)$proximo['rumo'];
                } else {
                    $somaRumos = (float)$proximo['rumo'];
                    $rumoPontoAnterior = (float)$proximo['rumo'] * 1;
                }

                $pontoTeste = $Vincenty->funcaoDiretaVicenty($latitudePontoAnterior, $longitudePontoAnterior, $proximo['distancia'], $somaRumos);
                $proximo['lat'] = $pontoTeste[2];
                $proximo['long'] = $pontoTeste[3];
                if ($proximo['desenhar'] == true) {
                    $pontoAuxiliar = [
                        'latitude' => $proximo['lat'],
                        'longitude' => $proximo['long'],
                        'altitude' => $proximo['altitude']
                    ];
                    $retorno[] = $pontoAuxiliar;
                }
            }
            $latitudePontoAnterior = $proximo['lat'];
            $longitudePontoAnterior = $proximo['long'];
        }
// return compact('caminho','retorno');
        return $retorno;
    }

    public function rumoDistanciaUniao2Array(array $caminho1, array $caminho2): array
    {
        $parte1 = $this->rumoDistancia($caminho1);
        $parte2 = $this->rumoDistancia($caminho2);
        $reversed = array_reverse($parte2);
        $result = array_merge($parte1, $reversed);

        return $result;
    }

}
