<?php

namespace Atd\Calculator\Services;

use League\Geotools\Coordinate\Ellipsoid;
use Toin0u\Geotools\Facade\Geotools;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class GeraObjetosGeo
{

    public function utm($lat, $long)
    {
        $montando = $lat . ',' . $long;
        $retorno = [];
        if ($long && $lat) {

            $coordinate = Geotools::coordinate($montando);
            $converted = Geotools::convert($coordinate);
//// convert to decimal degrees without and with format string
            $toDecimalMinutes = $converted->toDecimalMinutes(); // 40 26.7717N, -79 56.93172W
            $retorno['toDecimalMinutes'] = $toDecimalMinutes;
            $toDM = $converted->toDM('%P%D°%N %p%d°%n'); // 40°26.7717 -79°56.93172
            $retorno['toDM'] = $toDM;
//// convert to degrees minutes seconds without and with format string
//        printf("%s\n", $converted->toDegreesMinutesSeconds('<p>%P%D:%M:%S, %p%d:%m:%s</p>')); // <p>40:26:46, -79:56:56</p>
            $toDMS = $converted->toDMS(); // 40°26′46″N, 79°56′56″W
            $retorno['toDMS'] = $toDMS;
//// convert in the UTM projection (standard format)
            $toUniversalTransverseMercator = $converted->toUniversalTransverseMercator(); // 17T 589138 4477813
            $retorno['toUniversalTransverseMercator'] = $toUniversalTransverseMercator;
            $toUTM = $converted->toUTM(); // 17T 589138 4477813 (alias)
            $retorno['toUTM'] = $toUTM;
            $pieces = explode(" ", $toUTM);
            $retorno['UTM_0'] = $pieces[0];
            $retorno['UTM_1'] = $pieces[1];
            $retorno['UTM_2'] = $pieces[2];
        }

        return $retorno;
    }

    public function coordenada($valor)
    {

        $GeraObjetosGeo = new GeraObjetosGeo();
        $latitude = $valor->getLat();

        $arrayLatitude = $GeraObjetosGeo->DDtoDMS($latitude, "latitude");

        $latG = $arrayLatitude['deg'];
        $latM = $arrayLatitude['min'];
        $latS = $arrayLatitude['sec'];
        $latGMS = $arrayLatitude['GMS'];


        $longitude = $valor->getLng();

        $arrayLongitude = $GeraObjetosGeo->DDtoDMS($longitude, "longitude");

        $lngG = $arrayLongitude['deg'];
        $lngM = $arrayLongitude['min'];
        $lngS = $arrayLongitude['sec'];
        $lngGMS = $arrayLongitude['GMS'];


        return ["latitude" => $latitude,
            "longitude" => $longitude,
            "latG" => $latG,
            "latM" => $latM,
            "latS" => $latS,
            "lngG" => $lngG,
            "lngM" => $lngM,
            "lngS" => $lngS,
            "latGMS" => $latGMS,
            "lngGMS" => $lngGMS
        ];
    }

    public function DDtoDMS($dec, $latlng)
    {
        if (!$dec) {
            return array("deg" => 0, "min" => 0, "sec" => 0, "GMS" => 0);
        }
        // Converts decimal format to DMS ( Degrees / minutes / seconds )
        $vars = explode(".", $dec);
        $deg = $vars[0];
        $tempma = 0;
        if (isset($vars[1])) {
            $tempma = "0." . $vars[1];
        }

        $tempma = $tempma * 3600;
        $min = floor($tempma / 60);
        $sec = $tempma - ($min * 60);

        if ($latlng == "longitude") {
            $complemento = "W";
        } elseif ($dec > 0) {
            $complemento = "N";
        } else {
            $complemento = "S";
        }

        $gms = abs($deg) . "° " . $min . "' " . number_format($sec, 2, ',', '') . '" ' . $complemento;


        return array("deg" => abs($deg), "min" => $min, "sec" => number_format($sec, 2, ',', ''), "GMS" => $gms);
    }

    public function poligono($vertices)
    {
        if (!$vertices) {
            return null;
        }
        $unindoPontos = [];

        foreach ($vertices as $value) {
            $unindoPontos[] = new Point($value['latitude'], $value['longitude']);
        }

        $retorno = new Polygon([new LineString($unindoPontos)]);

        return $retorno;
    }

    public function poligonoSplit($poligono)
    {
        if (!$poligono) {
            return $poligono;
        }
        $retorno = [];
//        dd($poligono);
        foreach ($poligono as $i => $linestring) {
            $separandoPontos = explode(',', (string)$linestring);
            foreach ($separandoPontos as $ponto) {
                $separandoLatLong = explode(' ', (string)$ponto);

                $retorno[] = ['longitude' => $separandoLatLong[0],
                    'latitude' => $separandoLatLong[1],
                    'utm' => $this->utm($separandoLatLong[1], $separandoLatLong[0])];
            }
        }
        return $retorno;
    }

}
