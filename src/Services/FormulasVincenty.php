<?php

namespace Atd\Calculator\Services;

//use League\Geotools\Coordinate\Ellipsoid;
use Toin0u\Geotools\Facade\Geotools;

class FormulasVincenty
{

    public function finalBearing($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        $latA = deg2rad($latitudeTo);
        $latB = deg2rad($latitudeFrom);
        $dLng = deg2rad($longitudeFrom - $longitudeTo);

        $y = sin($dLng) * cos($latB);
        $x = cos($latA) * sin($latB) - sin($latA) * cos($latB) * cos($dLng);

        return (float)fmod((fmod(rad2deg(atan2($y, $x)) + 360, 360) + 180), 360);
    }

    public function initialBearing($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        $latA = deg2rad($latitudeFrom);
        $latB = deg2rad($latitudeTo);
        $dLng = deg2rad($longitudeTo - $longitudeFrom);

        $y = sin($dLng) * cos($latB);
        $x = cos($latA) * sin($latB) - sin($latA) * cos($latB) * cos($dLng);

        return (float)fmod((rad2deg(atan2($y, $x)) + 360), 360);
    }


    public function funcaoDiretaVicenty($lat1, $lon1, $dist, $brng)
    {
        if ($lat1 != null && $lon1 != null && $dist != null) {
            $coordenadaAntes = [$lat1, $lon1];
            $coordenada = Geotools::coordinate($coordenadaAntes);

            $destinationPoint = Geotools::Vertex()->setFrom($coordenada)->destination($brng, $dist);
            $converted = Geotools::convert($destinationPoint);
            $DMS = $converted->toDMS();
            $splitando = explode(', ', $DMS);
            $latitude = $splitando[0];
            $longitude = $splitando[1];

            $point = Geotools::vertex()->setFrom($coordenada)->setTo($destinationPoint);
            $brng2 = $this->finalBearing($lat1, $lon1, $destinationPoint->getLatitude(), $destinationPoint->getLongitude());
            $latitudeDecimal = $destinationPoint->getLatitude();
            $longitudeDecimal = $destinationPoint->getLongitude();

            return [$latitude, $longitude, $latitudeDecimal, $longitudeDecimal, $brng2];
        }
    }

    public function coordenadasDistancias($lat1, $long1, $lat2, $long2)
    {
        if (($lat1 != null &&
                $long1 != null &&
                $lat2 != null &&
                $long2 != null) &&
            (($lat1 != $lat2) ||
                ($long1 != $long2))) {

            $coordenadaAntes1 = [$lat1, $long1];
            $coordenada1 = Geotools::coordinate($coordenadaAntes1);

            $coordenadaAntes2 = [$lat2, $long2];
            $coordenada2 = Geotools::coordinate($coordenadaAntes2);

            $distance = Geotools::distance()->setFrom($coordenada1)->setTo($coordenada2);
//            $point    =  Geotools::vertex()->setFrom($coordenada1)->setTo($coordenada2);

            $brng2 = $this->finalBearing($lat1, $long1, $lat2, $long2);
            $brng1 = $this->initialBearing($lat1, $long1, $lat2, $long2);

            $juntando = [];
            $juntando[0] = $distance->in('m')->vincenty();
            $juntando[1] = $brng2;
            $juntando[2] = $brng1;
            $juntando[3] = $brng1;
            return $juntando;
        } else {
            $juntando = [0, 0, 0, 0];
            return $juntando;
        }
    }


}
